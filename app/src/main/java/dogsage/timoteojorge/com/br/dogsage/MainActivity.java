package dogsage.timoteojorge.com.br.dogsage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private EditText textBox;
    private Button ageButton;
    private TextView resultAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textBox = (EditText) findViewById(R.id.text_number_id);
        ageButton = (Button) findViewById(R.id.button_age_id);
        resultAge = (TextView) findViewById(R.id.result_id);

        ageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String typedText = textBox.getText().toString();
                if(typedText.isEmpty()){

                    resultAge.setText("No number typed!");
                    return;
                }

                int typedValue = Integer.parseInt(typedText);
                int finalResult = typedValue * 7;

                resultAge.setText("The dogs age in human years is: " + finalResult + " years");
            }
        });
    }
}
